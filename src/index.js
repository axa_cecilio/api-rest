var express = require('express');

var env = process.env.NODE_ENV || 'development';
          process.env.NODE_ENV = process.env.NODE_ENV || env;

var app = express(),
    fs = require('fs');

var config = require(__dirname + '/config')[env];

require(__dirname +'/config/express')(app, config);
require(__dirname +'/config/routes')(app);
var service = require(__dirname + '/config/service')(app, config);

service.init(config, function( datos ) {
    app.listen(config.port);
    console.log("Starting " + config.app.name + " - " + env + " mode, port: " + config.port);
});
