var express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session');

var VerifyToken  = require('../controllers/VerifyToken');

    
module.exports = function(app, config) {

    // Para gestionar, POST requests y cookies    
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded( {extended: true} ));

    // TODO: Leer secreto de fichero o generarlo aleatoriamente
    app.use(session({
      secret: config.secrets.session || 'key-super-secreta-sessions', 
      resave: true, 
      saveUninitialized: true
    }));

    app.use(function (req, res, next) {      
      // Middelware general
      next();
    });
};