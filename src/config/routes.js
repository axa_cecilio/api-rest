
var VerifyToken  = require('../controllers/VerifyToken');

let is_user  = function(req,res,next) {
    if (!req.user) 
        return res.status(403).send({ auth: false, message: 'Acceso inválido' });

    if (req.user.role !== "user" && req.user.role !== "admin") 
        return res.status(403).send({ auth: true, message: 'Acceso inválido' });

    next();
};
let is_admin  = function(req,res,next) {
    if (!req.user) 
        return res.status(403).send({ auth: false, message: 'Acceso inválido' });
    
    if (req.user.role !== "admin") 
        return res.status(403).send({ auth: true, message: 'Acceso inválido' });

    next();
};

module.exports = function(app) {

    app.all('*', function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        //res.header("Access-Control-Allow-Headers", "X-Requested-With");
        res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
        res.header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
        next();
    });
    
    var clients  = require('../controllers/client.controller');

    app.get('/api/client/id/:idClient',   VerifyToken,  is_user, clients.by_id);    
    app.get('/api/client/name/:clientName', VerifyToken, is_user, clients.by_name);
    app.get('/api/client/policy/:idPolicy', VerifyToken, is_admin, clients.by_policy);    

    var policies  = require('../controllers/policy.controller');
    app.get('/api/policies/:clientName', VerifyToken, is_admin, policies.by_name);    

    var auth  = require('../controllers/auth.controller');
    app.post('/auth/login',  auth.login);    
    app.get('/auth/login/:id',  auth.login_id);    
    
    app.get('/auth/logout', auth.logout);    
    app.get('/auth/user', VerifyToken, auth.user);    

    app.get('*', function (req, res) {
        // Intercepta el resto de peticiones
        res.status(403).jsonp({});
    });
};


 