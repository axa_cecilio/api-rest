var path = require('path');
var rootPath = path.normalize(__dirname + '/../../');

 
module.exports = {
    development: {        
        secrets: {
            session: 'peladillas'
        },        
        rootPath: rootPath,
        host: process.env.HOST || "http://localhost",
        port: process.env.PORT || 5577,                        
        urls:{ 
            'clients':'http://www.mocky.io/v2/5808862710000087232b75ac',
            'policies':'http://www.mocky.io/v2/580891a4100000e8242b75c5'
        },
        app: {
            name: 'Clients && Policies',
            log: 'clients_policies.log',        
            data: {
                clients:{},
                polities:{}
            },            
        },
    }
};