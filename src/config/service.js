const fs = require('fs');
const path = require('path');
const request = require('request');
const async=require('async');
const rootPath = path.normalize(__dirname + '/../../');

let CLIENTS = {};
let POLICIES = {};
let CLIENT_NAMES = {};

module.exports = function(app, config, next) {  

  var tasks = {
    fetch_clients: function (callback) {   
      var url = config['urls']['clients'];
      request(config['urls']['clients'], 
              function (error, response, body) {
                if (!error && response.statusCode == 200) {
                  var data = JSON.parse(body);
                  if (data.clients && data.clients.length) {
                    for(let i = 0; i < data.clients.length; i++) {
                      let cli = {
                        id: data.clients[i].id,
                        emailAddress: data.clients[i].email,
                        role: data.clients[i].role,
                        name: data.clients[i].name,
                        policies: {}
                      } 
                      CLIENTS[cli.id] = cli;
                      CLIENT_NAMES[cli.name] = cli.id;
                    }
                  }
                  callback(null, {by_id:CLIENTS, by_name:CLIENT_NAMES   });

                } else  
                callback(null, []);
            });
          },
      fetch_policies: function (callback) {            
        var url = config['urls']['policies'];
        request(config['urls']['policies'], function (error, response, body) {
          if (error) callback(error, []);

          if (response.statusCode == 200) {
            var data = JSON.parse(body);
            if (data.policies && data.policies.length) {
                for(let i = 0; i < data.policies.length; i++) {
                  _client = CLIENTS[data.policies[i].clientId];
                  let policy = {
                    id: data.policies[i].id,
                    email: data.policies[i].email,
                    inceptionDate: data.policies[i].inceptionDate,
                    installmentPayment: data.policies[i].installmentPayment,
                    clientId: data.policies[i].clientId,         
                    client: _client
                  }    
                  POLICIES[policy.id] = policy;
                }
            }            
            callback(null, POLICIES);
          } else  
            callback({message:'Bad request ' }, []);
      });
    }
  }
 
  return {
    policies: POLICIES,
    clients: CLIENTS,
    clients_names: CLIENT_NAMES,
    init: function(config ,done) {
      async.parallel(tasks,  function(err, results) {          
            if (err) {
              config.app.data['clients'] = [];
              config.app.data['clients_by_name'] = [];
              config.app.data['policies'] = [];
              done({
                policies: [],
                clients: [],
                clients_names: [],
              });
            }  else{
              config.app.data['clients'] = CLIENTS = results.fetch_clients.by_id
              config.app.data['clients_by_name'] = CLIENT_NAMES = results.fetch_clients.by_name
              config.app.data['policies'] = POLICIES = results.fetch_policies
              done({
                policies: results.fetch_policies,
                clients: results.fetch_clients.by_id,
                clients_names: results.fetch_clients.by_name,
              });
            }             
      });    
    }
  }
};