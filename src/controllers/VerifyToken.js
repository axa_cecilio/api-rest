var jwt = require('jsonwebtoken'); 
var config = require('../config')[process.env.NODE_ENV]

function verifyToken(req, res, next) {
  var token = req.headers['x-access-token'];

  if (!token && req.query.token) 
     token = req.query.token;

  if (!token) 
    return res.status(403).send({ auth: false, message: 'No token !' });

  jwt.verify(token, config.secrets.session, function(err, decoded) {      
    if (err) 
      return res.status(500).send({ auth: false, message: 'Token invalido' });    
          
    if (req.user_id  != decoded.id) {
        req.user_id  = decoded.id;      
        try {        
          req.user  = config.app.data['clients'][decoded.id];
        } catch(err) {
          req.user  = null;
          return res.status(500).send({ auth: false, message: 'User not found' });    
        }
    }
    next();
  });
}

module.exports = verifyToken;