
var jwt = require('jsonwebtoken'); 
var bcrypt = require('bcryptjs');

var config = require('../config')[process.env.NODE_ENV]



let get_client_by_id  =function(id) {   
    try {
        return config.app.data['clients'][id];
    } catch(err) {
        return null;
    }
};
let get_client_by_name  =function(name) {
    try {
        id = config.app.data['client_names'][name];
        return config.app.data['clients'][id];
    } catch(err) {
        return null;
    }
};
let get_client_by_policy  =function(id) {
    try {                
         let policy = config.app.data['policies'][id]        
         return policy.client;        
    } catch(err) {
        return null;
    }
};
 
module.exports = {
    login_id: function (req, res) {
        // TODO: gestionar contraseñas con bcrypt
        var user = {
            id: req.params.id 
        }
        // TODO: damos por bueno lo recibido en body
        var token = jwt.sign(user, config.secrets.session, {
          expiresIn: 86400 // 24 horas
        });
        console.log("Login token: " + token)
        res.status(200).send({ auth: true, token: token });        
    },      
    login: function (req, res) {
        // TODO: gestionar contraseñas con bcrypt
        var user = {
            id: req.body.id
        }
        // TODO: damos por bueno lo recibido en body
        var token = jwt.sign(user, config.secrets.session, {
          expiresIn: 86400 // 24 horas
        });
        console.log("Token: " + token)
        res.status(200).send({ auth: true, token: token });        
    },    
    logout: function (req, res) {        
        res.status(200).send({ auth: false, token: null });        
    },  
    user: function (req, res) {          
        res.status(200).send(req.user);        
    },  
};

