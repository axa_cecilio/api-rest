var config = require('../config')[process.env.NODE_ENV]

let get_client_by_id  =function(id) {   
    try {
        return config.app.data['clients'][id];
    } catch(err) {
        return null;
    }
};
let get_client_by_name  =function(name) {
    try {
        id = config.app.data['clients_by_name'][name];
        return config.app.data['clients'][id];
    } catch(err) {
        return null;
    }
};
let get_client_by_policy  =function(id) {
    try {                
         let policy = config.app.data['policies'][id]        
         return policy.client;        
    } catch(err) {
        return null;
    }
};

module.exports = {
    by_id: function (req, res) {
        let client = get_client_by_id(req.params.idClient);
        if (client) {
            res.status(200).jsonp(client);
        } else {
            res.status(404).jsonp({});
        }
    },    
    by_name: function (req, res) {
        let client = get_client_by_name(req.params.clientName);
        if (client) {
            res.status(200).jsonp(client);
        } else {
            res.status(404).jsonp({});
        }
    },    
    by_policy: function (req, res) {
        let client = get_client_by_policy(req.params.idPolicy);
        if (client) {
            res.status(200).jsonp(client);
        } else {
            res.status(404).jsonp({});
        }
    }    
};