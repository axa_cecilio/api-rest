var config = require('../config')[process.env.NODE_ENV]

let get_policies_by_name  =function(name) {
    try {
        let policies = [];
        for(id  in config.app.data['policies']) {
            let policy = config.app.data['policies'][id]
            if (policy.client.name === name) {
                policies.push(policy);
            }
        }
        return policies;
    } catch(err) {
        return [];
    }
};

module.exports = {
    by_name: function (req, res ) {
        let policies  = get_policies_by_name(req.params.clientName);
        if (policies) {
            res.status(200).jsonp(policies);
        } else {
            res.status(404).jsonp([]);
        }
    },        
};