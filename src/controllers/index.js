let  ClientsController = require('./client.controller');
let  PoliciesController = require('./policies.controller');

module.exports = {
  clientCtl: ClientsController,
  policyCtl: PoliciesController,
};