# PruebaNodeJs
Prueba Rest API NodeJs

Puntualizaciones:

* Las url se utilizaran como origen permanente de datos, es decir no solo las utilizaremos comopara obtener un fichero json y utlizarlo localmente para leer los datos, si no que se utilizará cada vez que se inicie la aplicación.

* A falta de contraseñas, o bien utilizamos una autenticación secilla basada en una contraseña preestablecida o bien habilito un endpoint donde posteriormente implementariamos la autenticación con un metodo elegido


1. En un primer acercamiento, establezco la estructura de carpetas teniendo en cuenta posibles modificaciones del proyecto.
   Creamos:
   * Carpeta docs para documentacion
   * Carpeta src para el codigo fuente.   (Hay código harcoded, que se implementaria en commits posteriores)

Version de Node JS utilizada 10.13.0

npm install   
node index.js


Para probar la API más facilmente he añadido la url /auth/login que permite obetener un token facilmente.

El middelware empleado para la gestion de tokens, buscará primero en la query de la petición, y si no lo encuentra buscaras en los headers de la misma.





